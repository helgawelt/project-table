var testArray = angular.module('testArray', ['ui.router']);
testArray.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('main', {
            url: "/main",
            templateUrl: "main.html"
        });
    $urlRouterProvider.otherwise('/main');
}]);

testArray.controller('NamesArray', function NamesArray($scope, $http) {
    $scope.postdata = function () {
		var data = {
			username: $scope.username,
			password: $scope.password
		};
    
        var config = {
            headers : {
                'Content-Type': 'application/json',
                'X-Appery-Database-Id': '5a7bc6960f0d315a149df572'
            }
        };
        $http.post('https://api.appery.io/rest/1/db/users', data, config) 
        .success(function (data, config) {
            $scope.PostDataResponse = data;
        })
        .error(function (data, config) {
            $scope.ResponseDetails = "Data: " + data;
        });
	};
    
    $scope.getdata = function () {
        $http({method: 'GET', url: 'https://api.appery.io/rest/1/db/login', params: {'username':$scope.users,'password': $scope.pass}, headers: {'X-Appery-Database-Id':'5a7bc6960f0d315a149df572'}})
        .success(function () {
            console.log('Welcome!');
        })
        .error(function () {
            console.log('error');
        });
    };
    
    
    
    
    
    $http({method: 'GET', url: 'https://api.appery.io/rest/1/db/collections/testCollection', headers: {'X-Appery-Database-Id':'58226eeee4b0a696f3532f3d'}, dataType: 'json'})
        .success(function (result) {
            console.log('success', result);
            $scope.objects = result;
        })
        .error(function (result) {
            console.log('error');
        });
});

/*testArray.run(["$rootScope", function($rootScope) {
    
    $rootScope.arrayObj = [
        {
           id: 1,
           username: 'Eric Lewis',
           gender: 'male',
           dob: '20.08.1980',
           email: 'erik@gmail.com'
        },
        {
           id: 2,
           username: 'Morgan Skinner',
           gender: 'male',
           dob: '17.10.1990',
           email: 'morgan@ukr.net'
        },
        {
           id: 3,
           username: 'Stella Wood',
           gender: 'female',
           dob: '02.06.1985',
           email: 'stella@yandex.ru'
        },
        {
           id: 4,
           username: 'Milton Walker',
           gender: 'male',
           dob: '12.09.1993',
           email: 'milton@gmail.com' 
        },
    ];
        
}]);

testArray.controller('NamesArray', function NamesArray($scope, $rootScope) {
    $scope.spliceArr = function(id) {
        for (i = 0; i < $rootScope.arrayObj.length; i++) {
            if ($rootScope.arrayObj[i].id == id) {
                $rootScope.arrayObj.splice(i, 1);
                break;
            }
        }
    };
});

testArray.controller('RadioController', function RadioController($rootScope) {
    $rootScope.RadioChange = function (s) {
        $rootScope.gender = s;
    };
});

testArray.controller('CreateArray', function CreateArray($scope, $rootScope, $state) {
    $rootScope.newId = function() {
        $scope.arrayId = [];
        for(i = 0; i < $rootScope.arrayObj.length; i++) {
            $scope.arrayId.push($rootScope.arrayObj[i].id);
        }
        for(i = 1; i < 1000; i++) {
            $scope.n = $scope.arrayId.indexOf(i);
            if ($scope.n == -1) {
                $rootScope.newArrayId = i;
                break;
            }
        }
    };
    $rootScope.newId();
    
    
    $rootScope.pushNewItem = function(firstName, lastName, gender, dob, email) {
       
        newObj = {
           id: $rootScope.newArrayId,
           username: firstName + ' ' + lastName,
           gender: gender,
           dob: dob,
           email: email
        };
        
        $rootScope.arrayObj.push(newObj);
        console.log($rootScope.arrayObj);
        $state.go("main");
    };
});

testArray.controller('UpdateArray', function UpdateArray($scope, $rootScope, $state, $stateParams) {
    $scope.name = $rootScope.arrayObj[$stateParams.id-1].username;
    $scope.gender = $rootScope.arrayObj[$stateParams.id-1].gender;
    $scope.nDate = $rootScope.arrayObj[$stateParams.id-1].dob;
    $scope.nEmail = $rootScope.arrayObj[$stateParams.id-1].email;
    
    $rootScope.updateItem = function(id, name, gender, nDate, nEmail) {
        
        
        updObj = {
            id: $rootScope.arrayObj[$stateParams.id-1].id,
            username: name,
            gender: gender,
            dob: nDate,
            email: nEmail
        };
                
        $rootScope.arrayObj[$stateParams.id-1] = updObj;
        console.log($rootScope.arrayObj);
                
                
        $state.go("main");
    };
});*/
